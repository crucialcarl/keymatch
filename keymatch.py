import requests
from bs4 import BeautifulSoup
import sys
import argparse
import datetime

#initial

def get_keywords(fhandle):
	with open(fhandle, 'rb') as fd:
		keywords = fd.read().splitlines()
	return keywords 

def get_pages(fhandle):
	with open(fhandle, 'rb') as fd:
		pages = fd.read().splitlines()
	return pages 

def find_keywords_in_pages(keywords,pages):
	global numb_matches
	total_pages = len(pages)
	pagecounter = 0
	for eachpage in pages:
		pagecounter = pagecounter + 1
		if args.verbose:
 			print("[*] [%s/%s] site: %s" % (pagecounter, total_pages, eachpage))
		thesoup = get_soup(eachpage)
		if thesoup != "":	
			thesouptext = thesoup.get_text()
			for keyword in keywords:
				if keyword in thesouptext:
					print("[*] Found keyword [%s] in site [%s]\n" % (keyword, eachpage)),
					save_soup_file(keyword, eachpage, thesouptext)
					numb_matches = numb_matches + 1

def save_soup_file(matched_keyword, on_page, soup_contents):
	nowstamp = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
	savename = on_page.translate(None,":/\.=-") + "_" + matched_keyword + "_" + nowstamp + '.txt'
	with open(savename, 'wb') as fd:
		fd.write(soup_contents.encode('ascii','replace'))
	if args.verbose:
		print("Saved: %s" % savename)
	

def get_soup(page):
	req_object = get_page(page)
	if req_object != "":
		page_contents = str(req_object.content).lower()
		soup = BeautifulSoup(page_contents)
		return soup
	else:
		return req_object
			
def get_page(page, ua="Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/5.0)"):
        headers = { "user-agent" : ua}
        if 'http' or 'https' in page:
                try:
                        r = requests.get(page, headers=headers, timeout=1.000)
                except:
			if args.verbose:
                        	print("[*] Unable to resolve or connect to site %s" % page)
                       	r = ""
        else:
		if args.verbose:
                	print("[*] Site must have http or https prefix)")
                	print("[*] %s is not valid" % page)
               	raise
        return r

def keymatch():
	parser = argparse.ArgumentParser(description="Determine if a list of keywords appears in a list of Websites")
	parser.add_argument("-k", "--keywords", help="file containing list of keywords")
	parser.add_argument("-s", "--sites", help="file containing list of sites")
	parser.add_argument("-v", "--verbose", action='store_true', help="full verbose, otherwise only matches are printed")


	global args
	args = parser.parse_args()
	global numb_matches
	numb_matches = 0

	ks = get_keywords(args.keywords)
	if args.verbose:
		print("\n[*] Selected keywords: %s" % ks)
	
	ps = get_pages(args.sites)
	
	find_keywords_in_pages(ks,ps)
	
	if args.verbose:
		print("\n[*] Found %s matches" % numb_matches)
	
if __name__ == "__main__":
	keymatch()
