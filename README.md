usage: keymatch.py [-h] [-k KEYWORDS] [-s SITES] [-v]

Determine if a list of keywords appears in a list of Websites

optional arguments:
  -h, --help            show this help message and exit

  -k KEYWORDS, --keywords KEYWORDS
                        file containing list of keywords

  -s SITES, --sites SITES
                        file containing list of sites

  -v, --verbose         full verbose, otherwise only matches are printed


 
note: Page contents are made lowercase prior to testing, so keywords list should be lower case.  
